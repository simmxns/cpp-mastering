/*
Escribir un programa que al digitar 3 numeros obtengas el mayor de ellos
*/

#include <iostream>
using namespace std;

int main()
{
    int n1, n2, n3;

    cout << "Cuales son tus tres numeros(?): " << endl;
    cin >> n1 >> n2 >> n3;
    if (n1 == n2 or n2 == n3 or n3 == n1) {
        cout << "Digita numeros diferentes";
    } else if (n1 > n2 and n2 < n1) {
        cout << "Tu primer numero es el mayor";
    } else if (n2 > n3 and n3 < n2) {
        cout << "Tu segundo numero es el mayor";
    } else {
        cout << "Tu tercer numero es el mayor";
    }

    return 0;
}
