/*
Digitar 4 numeros y que al digitar ese 4 me indique con cual de los
anteriores 3 es similar
*/

#include <iostream>
using namespace std;

int main()
{
    float n1, n2, n3, n4;

    cout << "Primer numero: ";
    cin >> n1;
    cout << "Segundo numero: ";
    cin >> n2;
    cout << "Tercer numero: ";
    cin >> n3;
    cout << "Cuarto numero: ";
    cin >> n4;

    if (n1 == n4) {
        cout << "Tu primer numero es igual al cuarto";
    } else if (n2 == n4) {
        cout << "Tu segundo numero es igual al cuarto";
    } else if (n3 == n4) {
        cout << "Tu tercer numero es igual al cuarto";
    } else {
        cout << "Tu cuarto numero no coincide con ninguno";
    }

    return 0;
}
