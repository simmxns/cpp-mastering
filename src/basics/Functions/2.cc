/*
Escriba una funcion llamada al_cuadrado() que calcule el cuadrado de un valor
que se transmite y despliegue el resultado. La funcion debera ser capaz de
elevar al cuadrado numeros flotantes
*/

#include <iostream>
#include <math.h>
#include <stdlib.h>
using namespace std;

void pedirDatos();
template <class rang>
void al_cuadrado(rang num);
float num;

int main()
{
    pedirDatos();
    cout.precision(3);
    al_cuadrado(num);

    //    system("pause");
    return 0;
}

void pedirDatos()
{
    cout << "Ingresar datos: ";
    cin >> num;
}
template <class rang>
void al_cuadrado(rang num)
{
    cout << pow(num, 2) << endl;
}
