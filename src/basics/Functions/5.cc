/*
Escriba una plantilla de funcion llamada despliegue() que despliegue
el valor del argumento unico que se le transmite cuando es llamada la funcion
*/

#include <iostream>
#include <stdlib.h>
using namespace std;

template <class rang>
void despliegue(rang algo);

int main()
{
    int algo1 = 8;
    float algo2 = 2.34;
    double algo3 = 456.214;
    char algo4[] = "Algo";

    despliegue(algo1);
    despliegue(algo2);
    despliegue(algo3);
    despliegue(algo4);

    //    system("pause");
    return 0;
}

template <class rang>
void despliegue(rang algo)
{
    cout << "Dato: " << algo << endl;
}
