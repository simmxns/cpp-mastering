/*
Una funcion realiza una tarea concreta y puede ser diseñada, implementada y
depura de manera indenpendiente al resto de condigo

tipo 'nombre'(tipo var1, tipo var2,...){
        conjunto de instrucciones;
    }
*/

// Ejemplo encontrar el mayor de 2 numeros
#include <iostream>
#include <stdlib.h>

using namespace std;

// Prototipo de funcion
int encontrarMax(int x, int y);

int main()
{
    int n1, n2;
    int mayor;

    cout << "Digite 2 numeros: ";
    cin >> n1 >> n2;

    mayor = encontrarMax(n1, n2);       // 5
    cout << "Mayor: " << mayor << endl; // mayor o encontrarMax(n1,n2)

    system("pause");
    return 0;
}

// Definicion de funcion
int encontrarMax(int x, int y)
{
    int numMax;

    if (x > y) {
        numMax = x;
    } else {
        numMax = y;
    }
    return numMax;
}