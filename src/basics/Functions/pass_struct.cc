// Paso de parametros de tipo estructura

#include <iostream>
using namespace std;

struct Persona {
    char nombre[30];
    int edad;
} p1;

void pedirDatos();
void mostrarDatos(Persona);

int main(int argc, char const *argv[])
{

    pedirDatos();
    mostrarDatos(p1);

    cin.ignore();
    cin.get();
    return 0;
}

void pedirDatos()
{
    cout << "Digite su nombre: ";
    cin.getline(p1.nombre, 30, '\n');
    cout << "Digite su edad: ";
    cin >> p1.edad;
}

void mostrarDatos(Persona p)
{
    cout << "\n\nNombre: " << p.nombre << endl;
    cout << "Edad: " << p.edad << endl;
}