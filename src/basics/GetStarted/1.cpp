/*
 * Escribe un programa que lea de la entrada estandar dos numeros
 * y muestre en la salida estandar su suma, resta, multiplicacion y division.
 */

/**
 * @simmxns
 */

#include <iostream>
using namespace std;

int main()
{
    // variables
    int n1, n2, suma = 0, resta = 0, multi = 0, divi = 0;

    // guardar variables n1 y n2
    cout << "Digita un numero: ";
    cin >> n1;
    cout << "Digita otro numero: ";
    cin >> n2;
    // operaciones matematicas basicas
    suma = n1 + n2;
    resta = n1 - n2;
    multi = n1 * n2;
    divi = n1 / n2;
    // mostrar resultados de las operaciones
    cout << "\nLa suma es: " << suma << endl;
    cout << "La resta es: " << resta << endl;
    cout << "La multiplicion es: " << multi << endl;
    cout << "La division es: " << divi << endl;

    cin.ignore();
    cin.get();
    return 0;
}
