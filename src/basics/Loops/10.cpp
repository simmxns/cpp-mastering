/*
Escriba un programa que calcule el valor de:
1!+!2+!3...+!n
(suma de factoriales)
*/

#include <iostream>
#include <stdlib.h>
using namespace std;

int main()
{
    int i, n, multi = 1, sum = 0;

    cout << "Valor de n: ";
    cin >> n;

    for (i = 1; i <= n; i++) {
        multi *= i;
        sum += multi;
    }

    cout << "Suma: " << sum << endl;

    system("pause");
    return 0;
}
