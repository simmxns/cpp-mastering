/*
Escriba un programa que lea valores enteros hasta que se introduzca
un valor en el rango [20-30] o se introduzca el valor 0. El programa
debe entregar la suma de los mayores a 0 introducidos
*/

#include <iostream>
using namespace std;

int main()
{
    int numero, suma = 0;

    do {
        cout << "Numero: ";
        cin >> numero;

        suma += numero;

    } while ((numero < 20) || (numero > 30) && (numero != 0));

    cout << "\nSuma: " << suma << endl;

    return 0;
}
