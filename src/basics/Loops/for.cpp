/*
-- SINTAXIS DE UN FOR --

for(expr1; expresion logica; expre2){
        conjunto de instrucciones;
}
*/

#include <conio.h>
#include <iostream>
using namespace std;

int main()
{
    int i;

    for (i = 1; i <= 10; i++) {
        cout << i << endl;
    }

    getch();
    return 0;
}
