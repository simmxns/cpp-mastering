/*
-- SINTAXIS DE UN WHILE --
*Primero piensa y luego actua*

while(){
        conjunto de instrucciones;
}
*/

#include <coneo.h>
#include <iostream>
using namespace std;

int main()
{
    int i;

    i = 10;

    while (i >= 1) {
        cout << i << endl;
        i--;
    }

    getch(); // tus .exe ya no se cierran automaticamente
    return 0;
}
