/*
-- COMIENZO DEL BLOQUE II -- OPERACIONES MATEMATICAS --

Escribe un programa que lea de la entrada estandar el precio de un
producto y muestre a la salida estandar el precio del producto al
aplicarse el IVA
*/

#include <iostream>
using namespace std;

int main()
{
    int p, iva, rdt = 0, pf = 0;
    // introduce el producto y el iva
    cout << "Producto: ";
    cin >> p;
    cout << "IVA: ";
    cin >> iva;
    // operaciones
    rdt = p * iva / 100;
    pf = rdt + p;
    // mostrar precio del iva y precio del producto final
    cout << "\nPrecio IVA: " << rdt << endl;
    cout << "Precio producto final: " << pf << endl;

    return 0;
}
