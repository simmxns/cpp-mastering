/*
Escribe la siguiente expresion matematica como expresion en C++
a + b / c + d
*/
#include <iostream>
using namespace std;

int main()
{
    // definiendo los valores a utilizar
    float a, b, c, d, resultado = 0;
    // haciendo que le usuario escriba los valores
    cout << "Valor de a: ";
    cin >> a;
    cout << "Valor de b: ";
    cin >> b;
    cout << "Valor de c: ";
    cin >> c;
    cout << "Valor de d: ";
    cin >> d;
    // operacion matematica
    resultado = (a + b) / (c + d);
    // redondear resultado
    cout.precision(3);
    // imprimir resultado
    cout << "Resultado: " << resultado << endl;

    return 0;
}
