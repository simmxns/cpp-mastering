/*
Escriba un programa que al digitar los valores de a b y c los resuelva con la
formula resolvente
*/

#include <iostream>
#include <math.h>
using namespace std;

int main()
{
    float a, b, c, resultadomas = 0, resultadomenos = 0;

    cout << "Valor de a: ";
    cin >> a;
    cout << "Valor de b: ";
    cin >> b;
    cout << "Valor de c: ";
    cin >> c;

    resultadomas = ((-b) + sqrt(pow((b), 2) - 4 * (a) * (c))) / (2 * (a));
    resultadomenos = ((-b) - sqrt(pow((b), 2) - 4 * (a) * (c))) / (2 * (a));

    cout << "\nResultado positivo: " << resultadomas;
    cout << "\nResultado negativo: " << resultadomenos << endl;

    return 0;
}
