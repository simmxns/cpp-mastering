/**
 * @file ejercicio1.cc
 * @details Ejecicio 1: Comprobar si un numero es par o impar, y señalar la
 * posicion de memoria donde se está guardando el numero. Con punteros.
 */

// * Señela el valor dentro de la memoria
// & Señala la posicion de memoria

#include <iostream>
using namespace std;

int main()
{
    int numero, *dir_numero;

    cout << "Digite un numero: ";
    cin >> numero;

    dir_numero = &numero; // Guardando la posicion de memoria

    if (*dir_numero % 2 == 0) {
        cout << "El numero: " << *dir_numero << " es par" << endl;
        cout << "Posicion de memoria: " << dir_numero << endl;
    } else {
        cout << "El numero: " << *dir_numero << " es impar" << endl;
        cout << "Posicion de memoria: " << dir_numero << endl;
    }

    printf("\n");
    system("pause");
    return 0;
}
