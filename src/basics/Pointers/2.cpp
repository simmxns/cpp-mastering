/** @details Determine if a number is prime of not with pointer and print the position of the memory that the typed number was allocated */

#include <iostream>

using std::cout, std::cin;

bool isPrime(int);

int main(int argc, char const *argv[])
{
    int num, *dirNum;

    cout << "Type a number: ";
    cin >> num;    

    dirNum = &num;
    auto result = isPrime(*dirNum) ? "Prime!" : "Compound or 1";

    cout << result << " | " << dirNum << "\n";

    return 0;
}

bool isPrime(int number)
{
    for (int i = 2; i < number; ++i)
        if (number % i == 0)
            return false;

    return number > 1;
}