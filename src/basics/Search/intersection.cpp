/*
Ordenamiento por INSERCCI�N

|5|3|4|1|2|
 |
 Comprueba si el elemento de su izquierda ya
 es menor que �l

|5|3|4|1|2|
   |
   Si numero izq > numero actual{
                        cambio
                }
*/

#include <iostream>
#include <stdlib.h>

using namespace std;
int main()
{
    int num[] = {4, 2, 3, 1, 5};
    int i, pos, aux;

    // Algoritmo del ordenamiento por inserccion
    for (i = 0; i < 5; i++) {
        pos = i;
        aux = num[i];

        while ((pos > 0) && (num[pos - 1] > aux)) {
            num[pos] = num[pos - 1];
            pos--;
        }
        num[pos] = aux;
    }

    cout << "Orden ASC: ";
    for (i = 0; i < 5; i++) {
        cout << num[i] << " ";
    }

    cout << "\nOrden DESC: ";
    for (i = 4; i > -1; i--) {
        cout << num[i] << " ";
    }
    cout << endl;

    system("pause");
    return 0;
}
