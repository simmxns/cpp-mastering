/*
Pedir al usuario 2 cadenas de caracteres de numeros,
uno entero y otro real, convertirlos a sus respectivos valores
y por ultimo sumarlos
*/

#include <iostream>
#include <stdlib.h>
#include <string.h>

using namespace std;
int main()
{
    char entero[100];
    char real[100];
    int ent;
    float rea, op = 0;

    cout << "Digite un numero entero: ";
    gets(entero);
    cout << "Digite un numero real: ";
    gets(real);

    ent = atoi(entero);
    rea = atof(real);
    op = ent + rea;

    cout << "Resultado: " << op << "\n";

    system("pause");
    return 0;
}
