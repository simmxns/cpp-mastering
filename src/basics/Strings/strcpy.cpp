/*
Copiar contenido de una cadena a otra - Funcion strcpy()
*/

#include <iostream>
#include <stdlib.h>
#include <string.h>

using namespace std;

int main()
{
    char nombre1[] = "Simon";
    char nombre2[20];

    strcpy(nombre2, nombre1); // y = x;

    cout << nombre2 << endl;

    system("pause");
    return 0;
}
