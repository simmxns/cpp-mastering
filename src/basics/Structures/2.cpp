/*
Hacer una estructura llamada alumno, en la cual se tendran
los siguientes campos: nombre, edad, promedio, pedir los datos
para 3 alumnos, comprobar cual de los 3 tiene el mejor promedio y
posteriormente imprimir los datos del alumno
*/

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
using namespace std;

struct alumnos {
    char nombre[30];
    int edad;
    int promedio;
} alumno[3];

int main()
{
    int mejor_prom = 0;
    int pos = 0;

    for (int i = 0; i < 3; i++) {
        cout << i + 1 << ").-" << endl;
        cout << "Nombre del alumno: ";
        fflush(stdin);
        cin.getline(alumno[i].nombre, 30, '\n');
        cout << "Edad del alumno: ";
        fflush(stdin);
        cin >> alumno[i].edad;
        cout << "Promedio del alumno: ";
        fflush(stdin);
        cin >> alumno[i].promedio;

        if (alumno[i].promedio > mejor_prom) {
            mejor_prom =
                alumno[i].promedio; // Guardamos el mayor promedio del alumno
            pos = i;                // Guardamos la posicion del alumno
        }
    }

    cout << "\nEl alumno con el mejor promedio" << endl;
    cout << "Nombre: " << alumno[pos].nombre << endl;
    cout << "Edad: " << alumno[pos].edad << endl;
    cout << "Promedio: " << alumno[pos].promedio << endl;

    system("pause");
    return 0;
}