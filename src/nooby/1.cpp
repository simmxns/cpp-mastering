// #2 dont use std::endl, endl isnt just give u a next line on ur output also
// flush the buffer and that take extra time instead use \n
#include <iostream>

void printRange(int start, int end)
{
    for (auto i = start; i != end; ++i)
        // BAD
        // std::cout << i << std::endl;

        // GOOD
        std::cout << i << '\n';
}
