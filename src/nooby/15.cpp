// #17 overuse magic numbers

float energy(float m)
{
  // introducing a basic constant in ur code can make it many times more readable
  // the compiler is going to optimize it away anyway, but pls just give it a good name, 4 the boys :)
  constexpr float SPEED_OF_LIGHT = 299792458.0;
  return m * SPEED_OF_LIGHT * SPEED_OF_LIGHT;
}