// #22 using totally unnecessary heap allocations when a stack allocation would have been fine
// head allocations are slow compared to slack allocations

#include <iostream>
#include <string>

struct Record
{
  int id;
  std::string name;

  Record(int id, std::string name) : id{id}, name{name} {}
};

void unnecessaryHeadAllocation()
{
  // did this really need to be a heap allocation?
  // lets just say fot the sake of argument that these objects are too big and u really do want them on heap
  Record *customer = new Record{0, "James"};
  Record *other = new Record{1, "Someone"};

  // do work...

  delete customer;
  delete other;
}

void necessaryHeadAllocation()
{
  // #23 not using unique pointer and shared pointer to do ur heap allocations

  // why dont we make a class that holds a pointer, and then in its destructor it deletes that pointer

  
  // u can give it a heap-allocated pointer and when it goes out of scope, it deletes it
  auto customer = std::unique_ptr<Record>(new Record{0, "James"});
  auto other = std::unique_ptr<Record>(new Record{1, "Someone"});
  
  // a shared pointer on the other hand uses reference-couting scheme
  // when the ref count hits zero as the last shared pointer goes out of scope that shared pointer is in change of the deletion
  auto customer2 = std::shared_ptr<Record>(new Record{0, "James"});
  auto other2 = std::shared_ptr<Record>(new Record{1, "Someone"});

  // #24 constructing a unique or shared pointer directly instead of using make unique or make shared
  // make_unique and make_shared will pass ur arguments directly to the constructor of ur object
  auto customer3 = std::make_unique<Record>(new Record{0, "James"});
  auto other3 = std::make_shared<Record>(new Record{1, "Someone"});

  // do work EXCEPTION
}
