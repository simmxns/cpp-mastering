// #25 any use of new or delete
#include <iostream>

struct SomeResource {};

// theres no reason to write functionality that already exists
class Widget
{
  public:
    Widget() : meta{std::make_unique<SomeResource>()}
    {
      // whatever 
    }

    /* 
    virtual ~Widget()
    {
      delete meta;
    }
    */

  private:
    std::unique_ptr<SomeResource> meta;
};
